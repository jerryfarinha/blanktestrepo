package com.blanklink.blanktest.tests;

import com.blanklink.blanktest.server.Server;
import com.blanklink.blanktest.daos.MessageDao;
import com.blanklink.blanktest.models.Message;
import org.codehaus.jackson.map.ObjectMapper;
import org.glassfish.grizzly.http.server.HttpServer;
import org.junit.*;
import org.skyscreamer.jsonassert.JSONAssert;

import static org.hamcrest.Matchers.*;


import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;

import java.util.List;

import static org.junit.Assert.assertThat;

/**
 * Created by jerry on 27/08/16.
 */
public class RestApiTest {
    private static HttpServer server;
    private WebTarget itemsTarget;
    private ObjectMapper objectMapper;
    private Message message1;
    private Message message2;


    @BeforeClass
    public static void beforeRestApiTestClass() {
        server = new Server().startServer();
    }

    @Before
    public void beforeRestApiTest() throws Exception {
        Client client = ClientBuilder.newClient();
        WebTarget target = client.target("http://localhost:8080/api/");
        itemsTarget = target.path("v1/items");
        objectMapper = new ObjectMapper();

        message1 = new Message();
        message1.setContent("Hello World");
        message2 = new Message();
        message2.setContent("Hello World 2");
    }

    @AfterClass
    public static void afterUserResourceTestClass() {
        server.shutdown();
    }

//    @Test
//    public void putMessageTestShouldSaveTheMessage() throws Exception {
//        String json = new ObjectMapper().writeValueAsString(message1);
//        itemsTarget.request().put(Entity.text(json));
//        Message actualMessage = MessageDao.getInstance().getMessage(1);
//        assertThat(actualMessage.getContent(), is(not(nullValue())));
//        assertThat(actualMessage.getContent(), is(equalTo(message1.getContent())));
//        MessageDao.getInstance().deleteAllMessages();
//    }

    @Test
    public void getMessageShouldReturnCorrectJson() throws Exception {
        MessageDao.getInstance().saveOrUpdateMessage(message1);
        String json = itemsTarget.path("/" + message1.getId()).request().get(String.class);
        JSONAssert.assertEquals("{id: " + message1.getId() + "}", json, false);
        JSONAssert.assertEquals("{content: \"" + message1.getContent() + "\"}", json, false);
        MessageDao.getInstance().deleteAllMessages();
    }

    @Test
    public void getAllMessagesShouldReturnListOfMessages() throws Exception {
        MessageDao.getInstance().saveOrUpdateMessage(message1);
        MessageDao.getInstance().saveOrUpdateMessage(message2);
        String json = itemsTarget.request().get(String.class);
        List<Message> actual = objectMapper.readValue(json, objectMapper.getTypeFactory().constructCollectionType(List.class, Message.class));
        assertThat(actual.size(), is(2));
        MessageDao.getInstance().deleteAllMessages();
    }
}
