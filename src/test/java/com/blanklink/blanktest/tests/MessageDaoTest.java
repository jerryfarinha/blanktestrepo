package com.blanklink.blanktest.tests;

import com.blanklink.blanktest.daos.MessageDao;
import com.blanklink.blanktest.models.Message;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

/**
 * Created by jerry on 27/08/16.
 */
public class MessageDaoTest{
    private Message message;
    private MessageDao messageDao;

    @Before
    public void beforeMessageDaoTest(){
        message = new Message();
        message.setContent("Hello World");
        messageDao = MessageDao.getInstance();
        messageDao.saveOrUpdateMessage(message);
    }

    @After
    public void afterMessageDaoTest(){
        messageDao.deleteAllMessages();
    }

    @Test
    public void getAllMessagesShouldReturnAllMessages() {
        assertTrue(messageDao.getAllMessages().size()>0);
        System.out.println(messageDao.getAllMessages().size());
    }
}
