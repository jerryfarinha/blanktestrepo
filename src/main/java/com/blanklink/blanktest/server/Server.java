package com.blanklink.blanktest.server;

import com.blanklink.blanktest.daos.MessageDao;
import com.blanklink.blanktest.models.Message;
import org.glassfish.grizzly.http.server.HttpHandler;
        import org.glassfish.grizzly.http.server.HttpServer;
        import org.glassfish.grizzly.http.server.StaticHttpHandler;
        import org.glassfish.jersey.grizzly2.httpserver.GrizzlyHttpServerFactory;
        import org.glassfish.jersey.server.ResourceConfig;
        import java.net.URI;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by jerry on 27/08/16.
 */
public class Server {
    public static final String BASE_API_URI = "http://localhost:8080/api/";
    public boolean getFileCacheEnabled() {
        return false;
    }
    public static void main(String[] args) throws Exception {
        Server server = new Server();
        final HttpServer httpServer = server.startServer();
        MessageDao messageDao;
        messageDao = MessageDao.getInstance();
        String messageContent = "Hello World";
        for(int i = 0 ; i < 10; i++){
            Message message = new Message();
            message.setContent(messageContent);
            messageDao.saveOrUpdateMessage(message);
            messageContent = message.getContent() +", and Again Hello";
        }
        System.out.println("Press enter to stop the server...");
        System.in.read();
        httpServer.shutdown();
    }
    public HttpServer startServer() {
        final ResourceConfig rc = new ResourceConfig().packages("com.blanklink.blanktest");
        HttpServer server = GrizzlyHttpServerFactory.createHttpServer(URI.create(BASE_API_URI), rc);
        server.getServerConfiguration().addHttpHandler(getHttpHandler(), "/page");
        return server;
    }
    public HttpHandler getHttpHandler() {
        StaticHttpHandler handler = new StaticHttpHandler("src/main/java/com/blanklink/blanktest/webapp/");
        handler.setFileCacheEnabled(getFileCacheEnabled());
        return handler;
    }
}