package com.blanklink.blanktest.services;

import com.blanklink.blanktest.daos.MessageDao;
import com.blanklink.blanktest.models.Message;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.annotate.JsonSerialize;
import org.glassfish.jersey.server.JSONP;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.util.List;

/**
 * Created by jerry on 27/08/16.
 */

@Path("v1/items")
@Produces(MediaType.APPLICATION_JSON)
public class MessageApi {

    private static final String ITEMS_URL = "/api/v1/items";

    @GET
    @JSONP(queryParam = "callback")
    public String getAllMessages(@QueryParam("offset") int offset,
                              @QueryParam("count") int count,
                              @QueryParam("callback") String callback) throws Exception {
        ObjectMapper mapper = new ObjectMapper();
        mapper.setSerializationInclusion(JsonSerialize.Inclusion.NON_DEFAULT);
        List<Message> messages = MessageDao.getInstance().getAllMessages(offset, count);
        for (Message message : messages) {
            message.setLink(ITEMS_URL + "/" + message.getId());
        }
        return mapper.writeValueAsString(messages);
    }

    @GET
    @Path("/{id}")
    @JSONP(queryParam = "callback")
    public String getMessage(@PathParam("id") int id) throws Exception {
        Message message = MessageDao.getInstance().getMessage(id);
        return new ObjectMapper().writeValueAsString(message);
    }

    @PUT
    @JSONP(queryParam = "callback")
    public void putMessage(String messageJson) throws Exception {
        Message message = new ObjectMapper().readValue(messageJson, Message.class);
        MessageDao.getInstance().saveOrUpdateMessage(message);
    }

    @DELETE
    @JSONP(queryParam = "callback")
    public void deleteAllMessages() throws Exception {
        MessageDao.getInstance().deleteAllMessages();
    }
}
