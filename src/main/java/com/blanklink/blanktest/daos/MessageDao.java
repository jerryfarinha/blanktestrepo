package com.blanklink.blanktest.daos;

import com.blanklink.blanktest.utils.HibernateUtil;
import com.blanklink.blanktest.models.Message;
import org.hibernate.Query;
import org.hibernate.Session;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * Created by jerry on 27/08/16.
 */
public class MessageDao {

    private static volatile MessageDao instance = null;

    private MessageDao() { }

    public static synchronized MessageDao getInstance() {
        if (instance == null) {
            instance = new MessageDao();
        }
        return instance;
    }

    public List<Message> getAllMessages() {
        return getAllMessages(0, 0);
    }

    public List<Message> getAllMessages(int firstResult, int maxResult) {
        List<Message> messages = new ArrayList<>();
        Session session = HibernateUtil.getSessionFactory().openSession();
        Query query = session.createQuery("select id, content from Message");
        query.setFirstResult(firstResult);
        query.setMaxResults(maxResult);
        @SuppressWarnings("unchecked")
        List allUsers = query.list();
        for (Iterator it = allUsers.iterator(); it.hasNext(); ) {
            Object[] messageObject = (Object[]) it.next();
            Message message = new Message((Integer) messageObject[0]);
            message.setId((int) messageObject[0]);
            message.setContent((String) messageObject[1]);
            messages.add(message);
        }
        session.close();
        return messages;
    }

    public Message getMessage(int id) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        Message message = (Message) session.get(Message.class, id);
        session.close();
        return message;
    }

    public void saveOrUpdateMessage(Message message) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        session.saveOrUpdate(message);
        session.flush();
        session.close();
    }

    public void deleteAllMessages() {
        Session session = HibernateUtil.getSessionFactory().openSession();
        session.createQuery("delete from Message").executeUpdate();
        session.close();
    }
}
